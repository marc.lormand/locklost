import logging
import numpy as np

from gwpy.segments import Segment

from .. import config
from .. import data

##############################################


def initial_alignment_check(event):
    """Checks to see if an initial alignment is needed based off the output of the ITM green cameras"""

    if config.IFO == 'L1':
        logging.info("followup is not configured or not applicable to LLO.")
        return

    if event.transition_index[0] > config.INIT_GRD_STATE[0]:
        logging.info('Guardian state is too high to check for initial alignment.')
        return

    mod_window = [config.INITIAL_ALIGNMENT_SEARCH_WINDOW[0], config.INITIAL_ALIGNMENT_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))

    itm_channels = data.fetch(config.ITM_CHANNELS, segment)

    dof1_wfs = data.fetch(config.ALS_WFS_DOF1, segment)
    dof2_wfs = data.fetch(config.ALS_WFS_DOF2, segment)
    dof3_wfs = data.fetch(config.ALS_WFS_DOF3, segment)

    for i in dof1_wfs:
        i = np.abs(i.data) * config.DOF_1_SCALE
        i = np.array([i])

    for q in dof2_wfs:
        q = np.abs(q.data) * config.DOF_2_SCALE
        q = np.array([q])

    for j in dof3_wfs:
        j = np.abs(j.data) * config.DOF_3_SCALE
        j = np.array([j])

    initial_alignment = False

    wfs_arr = np.concatenate((i, q, j), axis=0)
    wfs_max = np.amax(wfs_arr)

    if wfs_max < config.WFS_THRESH:

        for check_als in itm_channels:

            chan_name = check_als.channel
            check_als = check_als.data

            signal = np.abs(check_als)
            signal_max = np.amax(signal)

            if signal_max >= config.INITIAL_ALIGNMENT_THRESH:
                initial_alignment = True
                logging.info('{} is above the threshold.'.format(chan_name))

        if initial_alignment:
            event.add_tag('INITIAL_ALIGNMENT')
        else:
            logging.info('An initial alignment is not needed.')
    else:
        logging.info('ALS WFS have not yet converged.')
