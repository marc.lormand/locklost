import logging
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

##############################################

itmx = mpatches.Patch(color='r', label='ITMX')
itmy = mpatches.Patch(color='b', label='ITMY')
etmx = mpatches.Patch(color='orange', label='ETMX')
etmy = mpatches.Patch(color='k', label='ETMY')


def plot_color(string):
    if 'ITMX' in string:
        return 'r'
    elif 'ITMY' in string:
        return 'b'
    elif 'ETMX' in string:
        return 'orange'
    else:
        return 'k'


def check_violin(event):
    """Checks violin 1st, 2nd, and 3rd order violin modes

    """
    if config.IFO == 'H1':
        logging.info('NOT SET UP FOR LHO')

    plotutils.set_rcparams()

    mod_window = [config.VIOLIN_SEARCH_WINDOW[0], config.VIOLIN_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    VIOLIN_channels = data.fetch(config.VIOLIN_CHANNELS, segment)

    saturating = False
    for buf in VIOLIN_channels:
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        idxs = ((t-event.gps) < -2)
        if np.max(buf.data[idxs]) > config.VIOLIN_SAT_THRESH:
            saturating = True

    if saturating:
        # ADD H1 code here
        if config.IFO == 'L1':
            if event.transition_index[0] >= 1020:
                event.add_tag('VIOLIN')
    else:
        if config.IFO == 'L1':
            logging.info('VIOLINS are ok')

    gs = gridspec.GridSpec(2, 4)
    gs.update(wspace=0.5)
    fig = plt.figure(figsize=(22*3, 16*3))
    ax1 = fig.add_subplot(gs[0, :2])
    ax2 = fig.add_subplot(gs[1, :2])
    ax3 = fig.add_subplot(gs[1, 2:])
    for idx, buf in enumerate(VIOLIN_channels):
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        if any(substring in buf.channel for substring in ['MODE3', 'MODE4', 'MODE5', 'MODE6']):
            ax1.plot(
                t-event.gps,
                buf.data,
                label=buf.channel,
                alpha=0.8,
                lw=8,
                color=plot_color(buf.channel),
            )
        elif any(substring in buf.channel for substring in ['MODE11', 'MODE12', 'MODE13', 'MODE14']):
            ax2.plot(
                t-event.gps,
                buf.data,
                label=buf.channel,
                alpha=0.8,
                lw=8,
                color=plot_color(buf.channel),
            )
        else:
            ax3.plot(
                t-event.gps,
                buf.data,
                label=buf.channel,
                alpha=0.8,
                lw=8,
                color=plot_color(buf.channel),
            )
    ax1.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax2.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax3.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax1.set_ylabel('Violin Magnitude [log]')
    ax2.set_ylabel('Violin Magnitude [log]')
    ax3.set_ylabel('Violin Magnitude [log]')
    ax1.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    ax2.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    ax3.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    if config.IFO == 'L1':
        ax1.set_ylim(-18, -14)
        ax2.set_ylim(-18, -14)
        ax3.set_ylim(-18, -14)

    ax1.legend(handles=[itmx, itmy, etmx, etmy], loc='best')
    ax2.legend(handles=[itmx, itmy, etmx, etmy], loc='best')
    ax3.legend(handles=[itmx, itmy, etmx, etmy], loc='best')
    # ax1.legend(loc='best')

    ax1.set_title('1st Order Violins')
    ax2.set_title('2nd Order Violins')
    ax3.set_title('3rd Order Violins')
    ax1.grid()
    ax2.grid()
    ax3.grid()

    outfile_plot = 'VIOLIN_monitor.png'
    outpath_plot = event.path(outfile_plot)
    fig.savefig(outpath_plot, bbox_inches='tight')
