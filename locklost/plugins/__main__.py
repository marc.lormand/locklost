import sys
import logging
import importlib

from .. import set_signal_handlers
from .. import config
from ..event import LocklossEvent


def main():
    set_signal_handlers()
    logging.basicConfig(
        level='DEBUG',
        format=config.LOG_FMT,
    )
    mpath = sys.argv[1].split('.')
    event = LocklossEvent(sys.argv[2])
    mod = importlib.import_module('.'+'.'.join(mpath[:-1]), __package__)
    func = mod.__dict__[mpath[-1]]
    func(event)


if __name__ == '__main__':
    main()
