% from locklost.web import utils

<br />
<div class="panel-group">
<div class="panel panel-default">

<div class="panel-heading">
% if section == "main":
    <h3 class="panel-title"><a data-toggle="collapse" href="#{{id}}">{{title}} (click to show)</a></h3>
% elif section == "sub":
    <h5 class="panel-title"><a data-toggle="collapse" href="#{{id}}">{{title}} (click to show)</a></h5>
% end
</div>
<br/>

% if expand == True:
    <div id="{{id}}" class="panel-collapse">
% else:
    <div id="{{id}}" class="panel-collapse collapse">
% end
<div class="panel-body">

<div class="row">
% try:
%   thumbnail_diff
%   thumbnails = []
%   for plot_url in plots:
%     thumbnails.append(utils.gen_thumbnail_url(plot_url))
%   end
% except:
%   thumbnails = plots
% end
% for plot_url, thumbnail_url in zip(plots, thumbnails):
    <div class="col-md-{{size}}">
    <div class="thumbnail">
    <a href="{{plot_url}}" target="_blank"><img src="{{thumbnail_url}}" style="width:100%" class="img-responsive" alt="" /></a>
    </div>
    </div>
    <br />
% end
</div>

</div>
</div>

</div>
</div>
