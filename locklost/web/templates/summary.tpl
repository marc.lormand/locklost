% rebase('base.tpl', IFO=IFO, web_script=web_script, online_status=online_status, is_home=is_home)

% import os

% from locklost import config

<h4>Summary Plots: {{epoch}}</h4>

% for id in [epoch]:
    <div id={{id}} class="tabcontent">
%   plot_dir = os.path.join(config.SUMMARY_ROOT, id)
%   for plotname in os.listdir(plot_dir):
%     plot_url = os.path.join(config.WEB_ROOT, 'summary', id, plotname)
%     include('plots.tpl', plots=[plot_url], size=8)
%   end
    </div>
% end
